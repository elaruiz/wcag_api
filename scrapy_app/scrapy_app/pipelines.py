# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

import json
from main.models import Protocolo, PaginaWeb, Analizador, Analisis

class ScrapyAppPipeline(object):
    def __init__(self, *args, **kwargs):
        self.items = []
        self.ids_seen = set()

    def process_item(self, item, spider):
        if item['codigo'] in self.ids_seen:
            pass
        else:
            self.ids_seen.add(item['codigo'])
            item.save()
        return item
        

class ScrapyPipeline(object):
    def __init__(self, *args, **kwargs):
        self.items = []
    @classmethod

    def process_item(self, item, spider):
        protocolo = Protocolo.objects.get(codigo=item['codigo'])
        pagina = PaginaWeb.objects.get(pk=item['idPagina'])
        analizador = Analizador.objects.get(pk=item['idAnalizador'])
        analisis = Analisis(
            created_at = item['created_at'],
            id_analizador = analizador,
            id_pag_web = pagina, 
            id_protocolo = protocolo,
            resultado = item['resultado']  
        )
        analisis.save()
        return item