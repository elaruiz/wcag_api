# -*- coding: utf-8 -*-
from scrapy.item import Field
from scrapy.item import Item
from scrapy.spiders import Spider
from scrapy.selector import Selector
from scrapy.loader import ItemLoader
from scrapy.loader.processors import MapCompose
from scrapy_app.items import WCAGItem

principios = ['perceptible', 'operable', 'comprensible', 'robusto']

class WcagSpider(Spider):
    name = 'wcag'
    start_urls = ['http://qweos.net/blog/2009/01/28/guias-practicas-para-profesionales-web-puntos-de-verificacion-de-las-pautas-de-accesibilidad-al-contenido-web-wcag-20/']
    custom_settings = {
        'ITEM_PIPELINES' : {
            'scrapy_app.pipelines.ScrapyAppPipeline': 300,
        }
    }

    def parse(self, response):

        sel = Selector(response)
        elem = sel.xpath('//*[@id="post-390"]/div[2]/table/tbody/tr')
      
        for cod in elem:
            item = WCAGItem()
            success= cod.xpath('.//td[1]/strong/a/text()').extract_first()
            criteria = " ".join(success.split()).split(" ", 1)
            item['codigo'] = criteria[0]
            item['criterio'] = criteria[len(criteria)-1]
            item['nivel'] = cod.xpath('.//td[2]/text()').extract_first()
            item['principio'] = principios[int(item['codigo'][0])-1]
            yield item

        