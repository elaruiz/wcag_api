# -*- coding: utf-8 -*-
import scrapy
import re
from scrapy.item import Field
from scrapy.item import Item
from scrapy.spiders import Spider
from scrapy.selector import Selector
from scrapy.loader import ItemLoader
from scrapy.loader.processors import MapCompose
from requests_toolbelt import MultipartEncoder

results= {"Failed":"FALLA", "Verify":"REVISION", "Passed": "BIEN"}

class QatarSpider(Spider):
    name = 'qatar'
    def __init__(self, *args, **kwargs):
        # We are going to pass these args from our django view.
        # To make everything dynamic, we need to override them inside __init__ method
        self.pagina = kwargs.get('pagina')
        self.idPagina = kwargs.get('idPagina')
        self.idAnalizador = kwargs.get('idAnalizador')
        self.time = kwargs.get('time')
        self.allowed_domains = ['qatar.checkers.eiii.eu']
        self.start_urls = ['http://qatar.checkers.eiii.eu/']
        super(QatarSpider, self).__init__(*args, **kwargs)
   
    def parse(self, response):
        data = {
            'url': self.pagina,
            'checker': 'wam'
        }
        yield scrapy.http.FormRequest(url='http://qatar.checkers.eiii.eu/', cookies={'storage_accepted':'true'}, formdata=data, callback=self.parse_response)

    def parse_response(self, response):
        sel = Selector(response)
        elem = sel.xpath('//*[@id="rstTestlist"]/li/span/a')

        for cod in elem:
            item = {}
            res = cod.xpath('.//span[contains(@class, "iconInLine")]/img/@alt').extract()
            for resp in enumerate(res):
                item['codigo'] = cod.xpath('.//span[2]/text()').re(r'\[(.*?)\]')[0]
                item['resultado'] = results[resp]
                item['idPagina'] = self.idPagina
                item['idAnalizador'] = self.idAnalizador
                item['created_at'] = self.time
                yield item