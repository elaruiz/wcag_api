# -*- coding: utf-8 -*-
import scrapy
from scrapy.item import Field
from scrapy.item import Item
from scrapy.spiders import Spider
from scrapy.selector import Selector
from scrapy.loader import ItemLoader
from scrapy.loader.processors import MapCompose
from requests_toolbelt import MultipartEncoder

results= {"AC_errors":"FALLA", "AC_potential_problems":"IMPOSIBLE", "AC_likely_problems":"REVISION"}

class AcheckerSpider(Spider):
    name = 'achecker'
    def __init__(self, *args, **kwargs):
        # We are going to pass these args from our django view.
        # To make everything dynamic, we need to override them inside __init__ method
        self.pagina = kwargs.get('pagina')
        self.idPagina = kwargs.get('idPagina')
        self.idAnalizador = kwargs.get('idAnalizador')
        self.time = kwargs.get('time')
        self.start_urls =  ['https://achecker.ca/checker/index.php']
        self.allowed_domains = ['achecker.ca']
        super(AcheckerSpider, self).__init__(*args, **kwargs)
   
    def parse(self, response):
        formdata = {
            'uri': self.pagina,
            'validate_uri': 'Check It',
            'MAX_FILE_SIZE': '52428800',
            'radio_gid[]': '9',
            'checkbox_gid[]': '8',
            'rpt_format': '1'
        }
        me = MultipartEncoder(fields=formdata)
        me_boundary = me.boundary[2:]  #need this in headers
        me_length = me.len             #need this in headers
        me_body = me.to_string()       #contains the request body 
        headers = {
            'Content-Type': 'multipart/form-data; boundary=' + me_boundary,
            }
        return scrapy.http.Request(url='https://achecker.ca/checker/index.php', method='POST', body=me_body, headers=headers, callback=self.parse_response)

    def parse_response(self, response):
        sel = Selector(response)
        elem = sel.xpath('//div[contains(@id, "AC_") and h4]')

        for cod in elem:
            for s in cod.xpath('.//h4'):
                item = {}
                item['codigo'] = s.xpath('.//text()').extract()[0].split(' ')[2]
                item['resultado'] = results[cod.xpath('.//@id').extract()[0]]
                item['idPagina'] = self.idPagina
                item['idAnalizador'] = self.idAnalizador
                item['created_at'] = self.time
                yield item