# -*- coding: utf-8 -*-
import scrapy
from scrapy.item import Field
from scrapy.item import Item
from scrapy.spiders import Spider
from scrapy.selector import Selector
from scrapy.loader import ItemLoader
from scrapy.loader.processors import MapCompose

results= {"Excelente": "BIEN", "Regular":"ADVERTENCIA", "Mal":"REVISION", "Muy mal":"FALLA"}

class AcheckerSpider(Spider):
    name = 'examinator'
    def __init__(self, *args, **kwargs):
        # We are going to pass these args from our django view.
        # To make everything dynamic, we need to override them inside __init__ method
        self.pagina = kwargs.get('pagina')
        self.idPagina = kwargs.get('idPagina')
        self.idAnalizador = kwargs.get('idAnalizador')
        self.start_urls =  ['http://examinator.ws']
        self.allowed_domains = ['examinator.ws']
        super(AcheckerSpider, self).__init__(*args, **kwargs)
   
    def parse(self, response):
        sel = Selector(response)
        x = sel.xpath('//*[@id="form1"]/form/p[2]/input[2]/@value').extract_first()
        data = {
            'url': self.pagina,
            'x': x,
        }
        yield scrapy.FormRequest.from_response(response, formxpath='//*[@id="form1"]/form', formdata=data, callback=self.parse_response)

    def parse_response(self, response):
        sel = Selector(response)
        item = {}
        item['dd'] = sel.extract()
        yield item