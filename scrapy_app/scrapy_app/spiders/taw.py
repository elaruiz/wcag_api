# -*- coding: utf-8 -*-
import scrapy
from scrapy.item import Field
from scrapy.item import Item
from scrapy.spiders import Spider
from scrapy.selector import Selector
from scrapy.loader import ItemLoader
from scrapy.loader.processors import MapCompose

results= {"Fail":"FALLA", "Pass":"BIEN", "Not checked":"IMPOSIBLE", "Unknown":"REVISION"}

class TawSpider(Spider):
    name = 'taw'
    def __init__(self, *args, **kwargs):
        # We are going to pass these args from our django view.
        # To make everything dynamic, we need to override them inside __init__ method
        self.pagina = kwargs.get('pagina')
        self.idPagina = kwargs.get('idPagina')
        self.idAnalizador = kwargs.get('idAnalizador')
        self.time = kwargs.get('time')
        self.start_urls =  ['https://www.tawdis.net']
        self.allowed_domains = ['tawdis.net']
        super(TawSpider, self).__init__(*args, **kwargs)
   
    def parse(self, response):
        data = {
            'url': self.pagina,
            'nivel': 'aaa',
            'crc': '0'
        }
        yield scrapy.http.FormRequest(url='https://www.tawdis.net/resumen', formdata=data, callback=self.parse_response)

    def parse_response(self, response):
        sel = Selector(response)
        elem = sel.xpath('//*[@class="principio"]/table/tbody/tr[td[@class="resultado"]/img]')
      
        for cod in elem:
            item = {}
            item['codigo'] = cod.xpath('.//td[@class="success_criteria"]/a/text()').extract_first()
            item['resultado'] = results[cod.xpath('.//td[@class="resultado"]/img/@alt').extract_first()]
            item['idPagina'] = self.idPagina
            item['idAnalizador'] = self.idAnalizador
            item['created_at'] = self.time
            yield item