"""crawlerWCAG URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url,static
from django.views.generic import TemplateView
from main import views
from django.conf.urls import include
from rest_framework import routers

router = routers.SimpleRouter()
router.register(r'wcag', views.ProtocoloView)
router.register(r'analizador', views.AnalizadorView)

urlpatterns = [
    url(r'^test$', views.AccessabilityTestView.post), 
    url(r'^last-test/(?P<pk>[a-z0-9]+)$', views.AccessabilityTestView.get),   
]

urlpatterns += router.urls