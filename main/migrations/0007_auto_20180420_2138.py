# Generated by Django 2.0.2 on 2018-04-21 01:38

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0006_auto_20180420_2013'),
    ]

    operations = [
        migrations.AlterField(
            model_name='analisis',
            name='id_analizador',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='analizador', to='main.Analizador'),
        ),
        migrations.AlterField(
            model_name='analisis',
            name='id_protocolo',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='protocolo', to='main.Protocolo'),
        ),
    ]
