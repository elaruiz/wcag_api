import requests

OAWKey = 'ee7bdcbb-eefa-4048-bc75-2061495e7f90'

def apiOAW(url):
    url = 'http://observatorioweb.ups.edu.ec/oaw/srv/wcag/json/conformidad/' 
    params = {'url': url, 'key': OAWKey}
    r = requests.get(url, params=params)
    results = r.json()
    return results

def apiCall(name, url):
    apis = {'OAW': apiOAW }
    return apis[name](url)