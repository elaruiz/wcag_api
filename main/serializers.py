from rest_framework import serializers
from django.db.models import Avg, Count, Min, Max, Sum, Case, When, IntegerField
from main.models import Protocolo, Analisis, Analizador, PaginaWeb

class ProtocoloSerializer(serializers.ModelSerializer):
    class Meta:
        model = Protocolo
        fields = ('id', 'codigo', 'criterio', 'principio', 'nivel')


class PaginaWebSerializer(serializers.ModelSerializer):
    class Meta:
        model = PaginaWeb
        fields = ('id', 'url', 'last_analysis')

class AnalisisSerializer(serializers.ModelSerializer):
    protocolo = ProtocoloSerializer(read_only=True, source = 'id_protocolo')
    class Meta:
        model = Analisis
        fields = ('id', 'created_at', 'resultado', 'protocolo')
        depth = 1

class AnalizadorSerializer(serializers.ModelSerializer):
    analisis = AnalisisSerializer(many=True, source = 'analizador')
    count_criteria = serializers.SerializerMethodField()
    count_level = serializers.SerializerMethodField()
    count_all = serializers.SerializerMethodField()
    count_guideline = serializers.SerializerMethodField()

    class Meta:
        model = Analizador
        fields = ('id', 'descripcion', 'nombre', 'activo', 'api', 'analisis', 'count_criteria', 'count_level', 'count_all', 'count_guideline')

    def get_count_criteria(self, obj):
         return obj.analizador.values('resultado').annotate(total=Count('resultado')).order_by('resultado')

    def get_count_level(self, obj):
         return obj.analizador.values('id_protocolo__nivel').annotate(total=Count('id_protocolo__nivel')).order_by('id_protocolo__nivel')
    
    def get_count_all(self, obj):
        return obj.analizador.values('id_protocolo__principio', 'resultado').order_by('id_protocolo__principio').aggregate(
            nivel_A_falla=Sum(
                Case(When(id_protocolo__nivel=Protocolo.NIVELA, resultado = Analisis.FALLA, then=1),
                    default = 0, output_field=IntegerField())
            ),
            nivel_A_bien=Sum(
                Case(When(id_protocolo__nivel=Protocolo.NIVELA, resultado = Analisis.OK, then=1),
                    default = 0, output_field=IntegerField())
            ),
            nivel_A_revision=Sum(
                Case(When(id_protocolo__nivel=Protocolo.NIVELA, resultado = Analisis.REVISION, then=1),
                    default = 0, output_field=IntegerField())
            ),
            nivel_A_imposible=Sum(
                Case(When(id_protocolo__nivel=Protocolo.NIVELA, resultado = Analisis.IMPOSIBLE, then=1),
                    default = 0, output_field=IntegerField())
            ),
            nivel_AA_falla=Sum(
                Case(When(id_protocolo__nivel=Protocolo.NIVELAA, resultado = Analisis.FALLA, then=1),
                    default = 0, output_field=IntegerField())
            ),
            nivel_AA_bien=Sum(
                Case(When(id_protocolo__nivel=Protocolo.NIVELAA, resultado = Analisis.OK, then=1),
                    default = 0, output_field=IntegerField())
            ),
            nivel_AA_revision=Sum(
                Case(When(id_protocolo__nivel=Protocolo.NIVELAA, resultado = Analisis.REVISION, then=1),
                    default = 0, output_field=IntegerField())
            ),
            nivel_AA_imposible=Sum(
                Case(When(id_protocolo__nivel=Protocolo.NIVELAA, resultado = Analisis.IMPOSIBLE, then=1),
                    default = 0, output_field=IntegerField())
            ),
            nivel_AAA_falla=Sum(
                Case(When(id_protocolo__nivel=Protocolo.NIVELAAA, resultado = Analisis.FALLA, then=1),
                    default = 0, output_field=IntegerField())
            ),
            nivel_AAA_bien=Sum(
                Case(When(id_protocolo__nivel=Protocolo.NIVELAAA, resultado = Analisis.OK, then=1),
                    default = 0, output_field=IntegerField())
            ),
            nivel_AAA_revision=Sum(
                Case(When(id_protocolo__nivel=Protocolo.NIVELAAA, resultado = Analisis.REVISION, then=1),
                    default = 0, output_field=IntegerField())
            ),
            nivel_AAA_imposible=Sum(
                Case(When(id_protocolo__nivel=Protocolo.NIVELAAA, resultado = Analisis.IMPOSIBLE, then=1),
                    default = 0, output_field=IntegerField())
            ),

        )
            
    def get_count_guideline(self, obj):
         return obj.analizador.values('id_protocolo__principio', 'resultado').order_by('id_protocolo__principio').aggregate(
            operable_falla=Sum(
                Case(When(id_protocolo__principio=Protocolo.OPERABLE, resultado = Analisis.FALLA, then=1),
                    default = 0, output_field=IntegerField())
            ),
            operable_bien=Sum(
                Case(When(id_protocolo__principio=Protocolo.OPERABLE, resultado = Analisis.OK, then=1),
                    default = 0, output_field=IntegerField())
            ),
            operable_revision=Sum(
                Case(When(id_protocolo__principio=Protocolo.OPERABLE, resultado = Analisis.REVISION, then=1),
                    default = 0, output_field=IntegerField())
            ),
            operable_imposible=Sum(
                Case(When(id_protocolo__principio=Protocolo.OPERABLE, resultado = Analisis.IMPOSIBLE, then=1),
                    default = 0, output_field=IntegerField())
            ),
            perceptible_falla=Sum(
                Case(When(id_protocolo__principio=Protocolo.PERCEPTIBLE, resultado = Analisis.FALLA, then=1),
                    default = 0, output_field=IntegerField())
            ),
            perceptible_bien=Sum(
                Case(When(id_protocolo__principio=Protocolo.PERCEPTIBLE, resultado = Analisis.OK, then=1),
                    default = 0, output_field=IntegerField())
            ),
            perceptible_revision=Sum(
                Case(When(id_protocolo__principio=Protocolo.PERCEPTIBLE, resultado = Analisis.REVISION, then=1),
                    default = 0, output_field=IntegerField())
            ),
            perceptible_imposible=Sum(
                Case(When(id_protocolo__principio=Protocolo.PERCEPTIBLE, resultado = Analisis.IMPOSIBLE, then=1),
                    default = 0, output_field=IntegerField())
            ),
            comprensible_falla=Sum(
                Case(When(id_protocolo__principio=Protocolo.COMPRENSIBLE, resultado = Analisis.FALLA, then=1),
                    default = 0, output_field=IntegerField())
            ),
            comprensible_bien=Sum(
                Case(When(id_protocolo__principio=Protocolo.COMPRENSIBLE, resultado = Analisis.OK, then=1),
                    default = 0, output_field=IntegerField())
            ),
            comprensible_revision=Sum(
                Case(When(id_protocolo__principio=Protocolo.COMPRENSIBLE, resultado = Analisis.REVISION, then=1),
                    default = 0, output_field=IntegerField())
            ),
            comprensible_imposible=Sum(
                Case(When(id_protocolo__principio=Protocolo.COMPRENSIBLE, resultado = Analisis.IMPOSIBLE, then=1),
                    default = 0, output_field=IntegerField())
            ),
            robusto_falla=Sum(
                Case(When(id_protocolo__principio=Protocolo.ROBUSTO, resultado = Analisis.FALLA, then=1),
                    default = 0, output_field=IntegerField())
            ),
            robusto_bien=Sum(
                Case(When(id_protocolo__principio=Protocolo.ROBUSTO, resultado = Analisis.OK, then=1),
                    default = 0, output_field=IntegerField())
            ),
            robusto_revision=Sum(
                Case(When(id_protocolo__principio=Protocolo.ROBUSTO, resultado = Analisis.REVISION, then=1),
                    default = 0, output_field=IntegerField())
            ),
            robusto_imposible=Sum(
                Case(When(id_protocolo__principio=Protocolo.ROBUSTO, resultado = Analisis.IMPOSIBLE, then=1),
                    default = 0, output_field=IntegerField())
            ),
 )

class PaginaSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    url = serializers.CharField()
    last_analysis=serializers.DateTimeField()
    has_tests = serializers.SerializerMethodField()
    @staticmethod
    def get_has_tests(instance):
        return Analisis.objects.filter(id_pag_web_id=instance.id).exists()