from urllib.parse import urlparse
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError
from django.core import serializers
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
from scrapyd_api import ScrapydAPI
from main.models import Protocolo, Analizador, PaginaWeb, Analisis
from django.db.models import Prefetch
from main.serializers import ProtocoloSerializer, AnalizadorSerializer, PaginaSerializer, AnalisisSerializer
from rest_framework import viewsets
from django.utils import timezone
from threading import Thread
from main.services import apiCall

# connect scrapyd service
scrapyd = ScrapydAPI('http://localhost:6800')

def is_valid_url(url):
    validate = URLValidator()
    try:
        validate(url) # check if url format is valid
    except ValidationError:
        return False
    return True

def scraping(analizador, pagina, time):
    var = analizador.get('nombre', None)
    # This is the custom settings for scrapy spider.
    # We can send anything we want to use it inside spiders and pipelines.
    # I mean, anything
    settings = {
        'USER_AGENT': 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'
    }
    task = scrapyd.schedule(
        'default', 
        var, 
        settings=settings, 
        pagina=pagina.data.get('url', None), 
        idPagina=pagina.data.get('id', None), 
        idAnalizador = analizador.get('id', None),
        time = time
    )
    status = 'pending'

    while status is not 'finished':
        status = scrapyd.job_status('default', task)
    return

class ProtocoloView(viewsets.ReadOnlyModelViewSet):
    queryset = Protocolo.objects.all()
    serializer_class = ProtocoloSerializer

class AnalizadorView(viewsets.ModelViewSet):
    queryset = Analizador.objects.all()
    serializer_class = AnalizadorSerializer

class AccessabilityTestView(APIView):
    @api_view(['POST'])
    def post(request):
        if request.method == 'POST':
            parse = request.data
            url = parse.get('url', None)
            if not url:
                return Response({'error': 'URL requerida'}, status=status.HTTP_400_BAD_REQUEST)
            if not is_valid_url(url):
                return Response({'error': 'La URL no es válida'}, status=status.HTTP_400_BAD_REQUEST)
        
            obj, created = PaginaWeb.objects.get_or_create(
                url=url,
                defaults={
                    'last_analysis': timezone.now()
                },
            )
            serializer = PaginaSerializer(obj)
            
            if created or not serializer.data.get('has_tests', False):
                obj.last_analysis = timezone.now()
                obj.save()
                analizadores = Analizador.objects.filter(activo = True)
                data = AnalizadorSerializer(analizadores, many=True)
                threads = []

                for pag in data.data:
                    if (pag.get('api', False)):
                        pass
                    else:
                        scrapy_thread = Thread(target=scraping, args=(pag,serializer, obj.last_analysis))
                        threads.append(scrapy_thread)
                # Ejecuta todos los hilos
                for x in threads:
                    x.start()
                # Espera que todos los hilos terminen
                for x in threads:
                    x.join()
                
                analisis = Analisis.objects.filter(created_at=obj.last_analysis)
                analizador = AnalizadorSerializer(
                    Analizador.objects.filter(analizador__created_at=obj.last_analysis).distinct().prefetch_related(
                        Prefetch('analizador', queryset=analisis)
                    ), many=True)
            
                return Response({'analizada': False, 'data': {'pagina': serializer.data, 'analisis': analizador.data}})
            
            elif (not created and serializer.data.get('has_tests', False)):
               data = {'id': obj.id, 'ultimo_analisis' : obj.last_analysis}
               return Response({'analizada': True, 'data': {'pagina': serializer.data, 'analisis': data}}) 
           
                
    @api_view(['GET'])
    def get(request, pk):
        if request.method == 'GET':
            id = pk
            pagina = PaginaWeb.objects.get(pk=id)
            serializer = PaginaSerializer(pagina)
            analisis = Analisis.objects.filter(id_pag_web=pagina.id).filter(created_at=pagina.last_analysis)
            analizador = AnalizadorSerializer(
                Analizador.objects.filter(analizador__id_pag_web=pagina.id).distinct().prefetch_related(
                    Prefetch('analizador', queryset=analisis)
                ), many=True)

            return Response({'data': {'pagina': serializer.data, 'analisis': analizador.data}})
            
