import json
from django.db import models
from django.utils import timezone

class Protocolo(models.Model):
    PERCEPTIBLE = 'perceptible'
    OPERABLE = 'operable'
    COMPRENSIBLE = 'comprensible'
    ROBUSTO = 'robusto'
    NIVELA = 'A'
    NIVELAA = 'AA'
    NIVELAAA = 'AAA'
    PRINCIPIOS = (
        (PERCEPTIBLE, 'Perceptible'),
        (OPERABLE, 'Operable'),
        (COMPRENSIBLE, 'Comprensible'),
        (ROBUSTO, 'Robusto'),
    )
    NIVELES = (
        (NIVELA, 'A'),
        (NIVELAA, 'AA'),
        (NIVELAAA, 'AAA'),
    )
    codigo = models.CharField(max_length=7, null=True)
    descripcion = models.TextField(null=True)
    principio = models.CharField(
        max_length=12,
        choices=PRINCIPIOS,
        null=True
    )
    criterio = models.CharField(max_length=100)
    nivel = models.CharField(
        max_length=3,
        choices=NIVELES,
        null=True
    )

class Analizador(models.Model):
    nombre = models.CharField(max_length=45)
    descripcion = models.CharField(max_length=256, null=True)
    activo = models.BooleanField()
    api = models.NullBooleanField()

class PaginaWeb(models.Model):
    url = models.CharField(max_length=250)
    last_analysis=models.DateTimeField(default=timezone.now)

class Analisis(models.Model):
    OK = 'BIEN'
    ADVERTENCIA = 'ADVERTENCIA'
    FALLA = 'FALLA'
    REVISION = 'REVISION'
    IMPOSIBLE = 'IMPOSIBLE'
    RESULTADO = (
        (OK, 'Bien'),
        (ADVERTENCIA, 'Advertencia'),
        (FALLA, 'Falla'),
        (REVISION, 'Requiere revisión manual'),
        (IMPOSIBLE, 'No se pudo comprobar'),
    )
    created_at = models.DateTimeField(default=timezone.now)
    id_analizador = models.ForeignKey(Analizador, on_delete=models.CASCADE, related_name='analizador')
    id_pag_web = models.ForeignKey(PaginaWeb, on_delete=models.CASCADE, related_name='analisis')
    id_protocolo = models.ForeignKey(Protocolo, on_delete=models.CASCADE, related_name='protocolo')
    linea = models.CharField(max_length=45, null = True)
    resultado = models.CharField(
        max_length=20,
        choices=RESULTADO,
        null=True
    )
    
    